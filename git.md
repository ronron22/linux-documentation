<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [git](#git)
    - [principe et remarque](#principe-et-remarque)
    - [Doc gitlab](#doc-gitlab)
        - [git et les branches](#git-et-les-branches)
            - [voir la branche actuelle](#voir-la-branche-actuelle)
            - [Créer une nouvelle branche s'appellant "pilotage"](#cr%C3%A9er-une-nouvelle-branche-sappellant-pilotage)
            - [setter les variables utilisateurs](#setter-les-variables-utilisateurs)
            - [commiter les modifs](#commiter-les-modifs)
            - [enfin les pousser](#enfin-les-pousser)
        - [cloner une branche spécifique d'un repo](#cloner-une-branche-sp%C3%A9cifique-dun-repo)
        - [afficher la liste des commits](#afficher-la-liste-des-commits)
        - [afficher un commit (ainsi que son diff)](#afficher-un-commit-ainsi-que-son-diff)
        - [voir les différences entre 2 branches](#voir-les-diff%C3%A9rences-entre-2-branches)
    - [poussez sa branche pour un merge](#poussez-sa-branche-pour-un-merge)
    - [pull de sa branche a partir de la master](#pull-de-sa-branche-a-partir-de-la-master)
        - [voir le diff entre deux commits](#voir-le-diff-entre-deux-commits)
        - [créer une branche à la volée](#cr%C3%A9er-une-branche-%C3%A0-la-vol%C3%A9e)
        - [supprimer une modification sur un fichier gité mais commité](#supprimer-une-modification-sur-un-fichier-git%C3%A9-mais-commit%C3%A9)
        - [supprimer un commit non pushé](#supprimer-un-commit-non-push%C3%A9)
        - [connaitre l'historique des modifications d'un fichier](#connaitre-lhistorique-des-modifications-dun-fichier)
        - [pousser un commit en particulier](#pousser-un-commit-en-particulier)
        - [retourner un fichier à un commit donné](#retourner-un-fichier-%C3%A0-un-commit-donn%C3%A9)
        - [retrouver une ancienne version de fichier](#retrouver-une-ancienne-version-de-fichier)
        - [ignorer des fichiers déjà ajouté (sans les supprimer)](#ignorer-des-fichiers-d%C3%A9j%C3%A0-ajout%C3%A9-sans-les-supprimer)
        - [doc à lire](#doc-%C3%A0-lire)
        - [clone d'un commit précis](#clone-dun-commit-pr%C3%A9cis)
        - [git stash](#git-stash)
        - [retirer un fichier qui a été "add"](#retirer-un-fichier-qui-a-%C3%A9t%C3%A9-add)
        - [git diff](#git-diff)
        - [git cherry-pick](#git-cherry-pick)
        - [afficher l'url "remote"](#afficher-lurl-remote)
        - [savoir si un fichier est présent dans l'index](#savoir-si-un-fichier-est-pr%C3%A9sent-dans-lindex)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# git

## principe et remarque

ne jamais créer de branche en production, car losque l'on va dessus, c'est celle-ci qui devient active

## Doc gitlab
http://docs.gitlab.com/ee/workflow/workflow.html

### git et les branches

#### voir la branche actuelle

```bash
git branch
* master
```

#### Créer une nouvelle branche s'appellant "pilotage" 

```bash
git branch pilotage
```bash

#### y aller

```bash
git checkout pilotage`
```bash

#### vérifier sa position

qu'on soit  bien sur la nouvelle branche 

```bash
git branch
    master
*  pilotage
```

#### setter les variables utilisateurs

```bash
git config --global user.name "Pouet Pouet"
git config --global user.email pouet@gruik.fr
```

#### commiter les modifs

```bash
git commit -m "add varnish steering(pilotage)"
```

#### enfin les pousser

```bash
git push origin pilotage
```

### cloner une branche spécifique d'un repo

```bash
git clone -b release2.0.0  git@git.ateway.fr:ansibleroles/aw-apache.git
```

### afficher la liste des commits

```bash
git log
```

### afficher un commit (ainsi que son diff) 

la dernière string est le n° de commit
 
```bash
git show 79e3efa2c4e87cb8bf4590ac13278b3e71a286b
```

### voir les différences entre 2 branches

```bash
git diff master pilotage
```

### poussez sa branche pour un merge

```bash
git push origin fix-1.6
```

### pull de sa branche a partir de la master

```bash
git branch --set-upstream-to=origin/master antonio-vhost
```

### voir le diff entre deux commits

obtenir le nom des commit avec *git log* puis afficher le diff avec

```bash
git diff 9d9d1d19 efb0404b06f
```

### créer une branche à la volée

```bash
git fetch origin master:mafeature
```

En faite idéalement :

1. tu créer une branch qui sera ta feature a partir du master de l'origin :

```bash
git fetch origin master:mafeature
```

2. tu va dessus

```bash
git checkout mafeature
```

3. tu tafff et tu commit pour valider.

4 tu fusionne tes commis pour éviter les commits genre "test test test ...." pour a voir un commit qui est un sens 🙂

```bash
git rebase -i origin/master
```

>squash tout les commits (sauf le premier)
> tu réécrit ton message cf : gitchangelog
5. tu push ta branch pour un merge;

```bash
git push origin mafeature
```

oui, le gitignore ce git
tant qu'il n'est pas commité, il ne s'applique pas pour les prochain commit

### supprimer une modification sur un fichier gité mais commité

```bash
git checkout -- data/system/all/hosts.j2
```

### supprimer un commit non pushé

```bash
git reset --hard HEAD~1
```

### connaitre l'historique des modifications d'un fichier

```bash
git log -p group_vars/ppdweb/rsyslog.yml
```

### pousser un commit en particulier

```bash
git push origin  fb459a826f7fc1d80f88d3e05ffe723790b8977c:mabranch
```

### retourner un fichier à un commit donné

retrouver le n° de commit

```bash
git log mon_fichier
```

revenir dessus

```bash
git checkout b13b7485887dfe655fbf03a30476c91228b9e7ea mon_fichier
```

### retrouver une ancienne version de fichier

Utiliser git log pour retrouver le commit qui nous intérèsse

puis utiliser *git show*

```bash
git show 96ff0b28092a3f113f0f4e35730f54ea50042847:create_new_site.sh
```

### ignorer des fichiers déjà ajouté (sans les supprimer)

```bash
git rm --cached
```

### localement récupérer les commits de master

```bash
git pull . master 
```

### doc à lire

https://bayol.pages.math.unistra.fr/tp-gitlab/git.html

### clone d'un commit précis

Il faut d'abord cloner la bonne branch puis faire un reset --hard vers le commit désiré

exemple

```bash
git reset --hard 112e36d4f7ba65387b5c7fcf15021d54b2e5c89f
```

### git stash

Vous devez naviguer entre différentes branches, mais vous ne voulez pas encore committer vos modifs et un message de git vous interdit alors le changement de branche.

La solution est la suivante :

```bash
git branch 
* tagada
* master
```

```bash
git stash # pour mettre en attente
git status # véifiez que rien n'est mis en attente de commit (l'interêt de git stash )
```

puis changer de branch et faire sa vie

Revenir sur sa branche de départ

```bash
git checkout tagada
git stash list # vérifions les "stash" en attente
git stash apply # récupérons les stash (les modifications mise en attente)
```

### retirer un fichier qui a été "add"

git reset HEAD fichier

### git diff

```bash
git diff
```

permet de voir les modifications non commitées
uté 

```bash
git diff n° de commit 
```

permet de voir les modifications commitées

### git cherry-pick

permet de pousser des commits vers une branche étrangère sans utiliser le mécanisme de merge qui fusionnerais l'ensemble des commits.

### afficher l'url "remote"

```bash
git remote get-url origin
```

### savoir si un fichier est présent dans l'index 

```bash
git ls-file
```

### quel sont les branches distantes

```bash
git ls-remote
```

### config par défaut de git

```bash
git config --list
```

### voir un commit donné

```bash
git show 2fcd4d755fc3d0a032e7a87c1314a2f52caf4326:ovh/api/browseapi-facto.py
```

### récupérer un commit donné

```bash
git checkout 2fcd4d755fc3d0a032e7a87c1314a2f52caf4326 browseapi-facto.py
```
