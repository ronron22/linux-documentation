# Puppet et les facts


le nom du fact ne rentre pas en compte, seul les clefs insérés à l'intérieur comptent.
La clé correspond est le nom réel du fact, celui qui apparaît lors de l'appel à puppet facts

### lister les facts (et les externals facts) (ancienne méthode)

```bash
facter -p
```

### Lister les facts avec puppet (nouvelle méthode)

```bash
puppet facts --render-as yaml
```

### Utilisation des facts dans les ressources

```bash
notify { 'greeting':
  message => "Am i virtual ? ${is_virtual}"
}
```

rendu

```bash
Notice: Am i virtual ? false
```

ou

```bash
$grr = $facts['foo_content']
notify {"rr: $grr": }
```

### La synchronisation des plugins et facts avec pluginsync

https://puppet.com/blog/introduction-pluginsync

Lors de l'execution des jobs, l'agent puppet récupère les plugins et facts du puppet master

## facts et modules

Les externals facts se mettent dans

```bash
facts.d
```

les customs facts dans

```bash
lib/facter
```

### ordres d'execution des facts

Les facts sont exécutés assez tôt semble t-il, avant le renvoi du catalogue côté client en tout cas).
Un de mes facts dépendant d'un script poussé par un manifest, renvoi une erreur lors de la première exécution car le script est alors absent du client.

### mise en debug d'un fact

```puppet
Facter::Type.newtype(:mycustomfact) do
...
  Facter.debug "blabla"
end
```

### tester un fact non présent dans le chemin habituel

```bash
export FACTERLIB="/tmp/:/home/blabla"
```
