# Traduction

## comment traduire en cli

### du français à l'anglois

```bash
apt -y install  translate-shell
```
Magiques !!!

```bash
trans :en "qui es-tu ?"  
qui es-tu ?
(null)

who are you?
/null/

Translations of qui es-tu ?
```

### de l'anglais au français

```bash
trans -s en -t fr forefront
```

créer un alias du genre *alias transen="trans -s en -t fr"*
