* [obtenir la date d'expiration d'un certificat, en ligne de commande] (#obtenir-la-date-d'expiration-d'un-certificat,-en-ligne-de-commande) 

https://www.sslshopper.com/article-most-common-openssl-commands.html

### obtenir la date d'expiration d'un certificat local, en ligne de commande

https://stackoverflow.com/questions/21297853/how-to-determine-ssl-cert-expiration-date-from-a-pem-encoded-certificate

```bash
openssl x509 -enddate -noout -in mycertif.pem
```

### obtenir la date d'expiration d'un certificat distant, en ligne de commande

```bash
`echo | openssl s_client -connect localhost:443 2>/dev/null | openssl x509 -noout -dates`
```

### obtenir le common name

```bash
openssl x509 -noout -subject -in cert.pem
```

### obtenir les Alternatives names

```bash
 openssl x509 -noout -text  -in /var/lib/dehydrated/certs/mugairyu.fr/cert.pem | grep DNS
```

### lire un csr - demande de signature

```bash
openssl req -text -noout -in summer-store.com.rsa.csr
```

### Où trouver les intermediates Globalsign

https://support.globalsign.com/customer/en/portal/articles/1223298-alphassl-intermediate-certificates

## déchffrer le ssl

pip3 install mitmproxy

+ 

sysctl etc..

+


openssl x509 -in .mitmproxy/mitmproxy-ca.pem -inform PEM -out mitm.crt
mkdir /usr/share/ca-certificates/extra/
cp -v mitm.crt /usr/share/ca-certificates/extra/
dpkg-reconfigure ca-certificates
iptables -t nat -A OUTPUT -p tcp --dport 443 -m owner ! --uid-owner 0 -j DNAT --to 127.0.0.1:8080 

mitmproxy --mode transparent --showhost
