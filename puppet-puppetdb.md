# Puppetdb

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [generate toc](#generate-toc)
- [documentation](#documentation)
- [Installation](#installation)
    - [Packages](#packages)
    - [database and user creation](#database-and-user-creation)
    - [verification](#verification)
    - [firewall](#firewall)
        - [Testing network access](#testing-network-access)
- [Configuration](#configuration)
    - [puppet master for using puppetdb](#puppet-master-for-using-puppetdb)
        - [install](#install)
            - [puppet master puppet.conf](#puppet-master-puppetconf)
            - [connecting puppet master on puppetdb](#connecting-puppet-master-on-puppetdb)
    - [Utilisation](#utilisation)
        - [Request a node](#request-a-node)
        - [Cleaning a node informations (facts, catalogue, etc..)](#cleaning-a-node-informations-facts-catalogue-etc)
    - [the CLI](#the-cli)
        - [installation](#installation)
        - [Query example](#query-example)
- [Troubleshooting](#troubleshooting)
    - [no catalog on the puppetdb server](#no-catalog-on-the-puppetdb-server)
        - [Fix](#fix)
    - [problem with ssl certificates](#problem-with-ssl-certificates)
        - [fix](#fix)
- [FaQ](#faq)
    - [Cleaning puppetdb](#cleaning-puppetdb)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## generate toc

```bash
doctoc puppet-puppetdb.md --gitlab
```

## documentation

https://puppet.com/docs/puppetdb/5.1/maintain_and_tune.html

## Installation

### Packages

```bash
apt install puppetdb postgresql-9.6
```

### database and user creation

```bash
su - postgres
createuser -DRSP puppetdb
createdb -E UTF8 -O puppetdb puppetdb
```

### verification

```bash
su - postgres
psql
\l
\du
```

You can find puppetdb database and the puppetdb user and his privileges

### firewall

Open the port 8082 or configure eventualy your haproxy for fowarded requests to the puppetdb port, don't forget to add acl for blocking the non authorized connections

#### Testing network access

```bash
nmap -P0 -p 8082 mypuppetdbnode
```

whether ok :

Open your browser on http://mypuppetdbnode:8082/pdb/dashboard/index.html for looking the performance puppetdb dashboard.

## Configuration

### puppet master for using puppetdb

#### install

```bash
puppet resource package puppetdb-termini ensure=latest
```

##### puppet master puppet.conf

```bash
cat  /etc/puppetlabs/puppet/puppet.conf 
[master]
...
# for using puppetdb
storeconfigs = true
storeconfigs_backend = puppetdb
reports = store,puppetdb
```

add route to /etc/puppetlabs/puppet/routes.yaml

```bash
cat  /etc/puppetlabs/puppet/routes.yaml
---
master:
  facts:
    terminus: puppetdb
    cache: yaml
```

and reload puppet server

##### connecting puppet master on puppetdb

Warning, this file must be create on *puppet config print confdir* directory

```bash
cat /etc/puppetlabs/puppet/puppetdb.conf 
[main]
server_urls = https://mypuppetdbnode:8081
```

### Utilisation

On the puppet master

#### Request a node

```bash
puppet node find tagada.com
```

should output a json fact

#### Cleaning a node informations (facts, catalogue, etc..)

```bash
puppet node clean mynodename
```

### the CLI

#### installation

```bash
puppet resource package puppet-client-tools ensure=latest
```

#### Query example

List all the nodes

```bash
puppet query -u "https://ns.architux.com:8081/" --cert=/etc/puppetlabs/puppet/ssl/certs/ns.architux.com.pem\  
--key=/etc/puppetabs/puppet/ssl/private_keys/ns.architux.com.pem  "nodes { certname ~ '.*' }"
```

Know nodes of the *mutu* role :

```bash
puppet query -u "https://ns.architux.com:8081/" --cert=/etc/puppetlabs/puppet/ssl/certs/ns.architux.com.pem\  
--key=/etc/puppetabs/puppet/ssl/private_keys/ns.architux.com.pem "nodes { certname in inventory[certname] { facts.role = 'mutu' }}"
```

with curl in ssl, get the puppet version

```bash
curl -k -X GET https://ns.architux.com:8081/pdb/query/v4 --tlsv1 --cert /etc/puppetlabs/puppet/ssl/certs/ns.architux.com.pem\
 --key /etc/puppetlabs/puppet/ssl/private_keys/ns.architux.com.pem --data-urlencode\
 'query=facts[value, count()] {name = "puppetversion" group by value}'

```

#### backup

```bash
puppet-db --cert=/etc/puppetlabs/puppet/ssl/certs/ns.architux.com.pem\
--key=/etc/puppetlabs/puppet/ssl/private_keys/ns.architux.com.pem\
-u https://ns.architux.com:8081  export pdb-archive.tgz
```

## Troubleshooting

launch *puppet agent -t --debug* for debugging

### no catalog on the puppetdb server

```bash
puppet agent -t
Warning: Unable to fetch my node definition, but the agent run will continue:
Warning: Error 500 on SERVER: Server Error: Could not retrieve facts for ns9.architux.com: Failed to find facts from PuppetDB at puppet:8140: Failed to execute '/pdb/query/v4/nodes/ns9.architux.com/facts' on at least 1 of the following 'server_urls': https://puppetdb:8081
Info: Retrieving pluginfacts
Info: Retrieving plugin
Info: Retrieving locales
Info: Loading facts
Error: Could not retrieve catalog from remote server: Error 500 on SERVER: Server Error: Failed to execute '/pdb/cmd/v1?checksum=217e706740b502533f6da0ec1d8c28553c4558bd&version=5&certname=ns9.architux.com&command=replace_facts&producer-timestamp=2019-02-02T12:01:37.758Z' on at least 1 of the following 'server_urls': https://puppetdb:8081
Warning: Not using cache on failed catalog
Error: Could not retrieve catalog; skipping run
```

####  Fix

https://tickets.puppetlabs.com/browse/PDB-3523

Nodes doesn't send their catalogn on the puppetdb server, go to performance dashboard for seen whether "Active Nodes" was populated 

consult /var/log/puppetlabs/puppetdb/puppetdb-access.log for watch whether there no authentication denied

#and configure puppet node for send catalogue like :  
#https://puppet.com/docs/puppetdb/5.1/connect_puppet_apply.html#step-2-install-terminus-plugins-on-every-puppet-node

in fact, it's

Warning, this file must be create on *puppet config print confdir* directory

move /etc/puppetlabs/puppetdb.conf in /etc/puppetlabs/puppet/ 

### problem with ssl certificates

...

#### fix

regenerate them with the puppetdb *ssl-setup* command

## FaQ

### Cleaning puppetdb 

https://ma.ttias.be/puppet-clearing-particular-node-puppetdb-exported-resources/

où 

https://makandracards.com/operations/49971-delete-a-node-from-puppet-and-puppetdb
