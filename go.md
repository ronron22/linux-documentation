echo "export GOPATH=$HOME/go" >> ~/.bash_profile
source ~/.bash_profile


### marshall

> unmarshal: decode
> marshal: encode

Toute variable ou type commencant par une majuscule est exportée

function : fonction
récepteur de fonction: variable associé au struct func (a A) nom()..
method : fonction ayant un récepteur comme paramètre

## parrallèle avec la POO

dans

```go
type Toto struct {
   Gruik string
}

func main () {
   var cochon Toto
...

var tt = Toto{"grr"}
```

* cochon est un objet et Toto une classe
tt initialise la struct

on defini une structure

### variables vs pointeurs

* une variable a un type (int, string etc..)
* un pointer n'a, de base, que le type de la variabl cible

### pointeurs

```go
a := 1
b := &a

fmt.Println(****b)

: 1
```

déférencer un pointeur est cette opération


