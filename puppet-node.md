# Puppet nodes

## ancienne méthode

La classe suivante :

```bash
class myprogram ($version) {
  package{ 'myprogram':
    ensure => $version
}
```

s'utilise dans site.pp (nodes), roles, profiles ou autres de la manière suivante :

```bash
class { 'myprogram':
  version => '2.1'
}
```

Cette syntaxe permet d'appeler la classe program tout en lui passant des paramètres, cela nous évite d'ajouter un :

```bash
include myprogram
```
