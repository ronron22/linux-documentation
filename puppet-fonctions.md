# puppet et fonctions

## Fonctions puppet

Deux type de fonctions classiques : 

* statement : qui ne retourne rien
* rvalue : qui retourne une valeur

Les fonctions ne sont exécutées que sur le serveur, donc elles ne peuvent interagir qu'avec les donnés chargées sur le serveur, pas sur les ondes.

### exemple de fonctions Puppet

https://www.example42.com/2015/10/07/puppet4-examples-functions/
