### find 500 errors

```bash
find_500.sh
test -z "$1" && echo "give logfile in argument" && exit 0 || awk '$10 ~ /50/  {print}' "$1"
```

### debugger apache

Mettre 

```bash
SetEnvIf X-Dn merguez DnDebug
Header always set X-Vhost "expr=%{SERVER_NAME}" env=DnDebug
Header always set X-Perf "delay %D time %t" env=DnDebug
```

puis appellez-le avec

```bash
curl -H "X-Dn: merguez" -I 50711ip51
HTTP/1.1 200 OK
Date: Wed, 17 Jul 2019 09:35:07 GMT
Server: Apache
Last-Modified: Wed, 05 Jun 2019 08:17:26 GMT
ETag: "19-58a8f3a47463f"
Accept-Ranges: bytes
Content-Length: 25
X-Vhost: 50711ip51
X-Perf: delay D=75 time t=1563356107801038
Connection: close
Content-Type: text/html
```

### voir les fichiers inclus

```bash
apache2ctl -t -D DUMP_INCLUDES
```

puis vérifier 

```bash
apache2ctl  -t -D DUMP_INCLUDES | grep munin
    (38) /etc/apache2/conf.d/munin.conf
```
