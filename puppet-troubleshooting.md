# Troubleshooting

### vérifier la syntaxe

```ruby
bundle exec rake syntax lint metadata_lint check:symlinks check:git_ignore check:dot_underscore check:test_file rubocop
```

### not in autoload module layout lors d'un appel à puppet-lint

(solution foireuse qui ne marche pas)

http://puppet-lint.com/checks/autoloader_layout/

Modifier le Rakefile du module afin d'ajouter :

```ruby
PuppetLint.configuration.send('disable_autoloader_layout,disable_relative')
```

## DSL

### duplicate declaration dans une iteration **each**

L'iteration va dupliquer le nom de la ressource, une erreur "circulaire" apparaîtra du genre

*duplicate on .. line 23  with on .. ligne 23*

Solution

Par exemple faire varier le nom en le suffixant de la variable issue de l'itération

Exemple

```puppet
$db_files.each |String $dbfile| {
      exec {"postfix.${dbfile}":
        command => "/usr/sbin/postmap ${configuration_directory}/${dbfile}",
        path        => ['/usr/bin', '/usr/sbin'],
        subscribe   => File["/etc/postfix/${dbfile}"],
        refreshonly => true,
      }
    }
```

## r10k

utiliser le mode verbeux

```bash
r10k deploy environnement -t -v
```

### fatal: could not read Username for 'https://github.com': No such device or address

```bash
ERROR	 -> Command exited with non-zero exit code:
Command: git --git-dir /var/cache/r10k/https---github.com-ronron22-puppet-module-ssh fetch origin --prune
Stderr:
fatal: could not read Username for 'https://github.com': No such device or address
Exit code: 128
```

Le dépôt n'existe plus, vérifier qu'il n'y a plus de référence sur le *Puppetfile* de **TOUTES** les branches !!

### Error: Found  dependency cycle

Exemple 

```bash
(File[/etc/postfix/main.cf] => File[/etc/postfix/main.cf])\nTry the '--graph' option and opening the resulting '.dot' file in OmniGraffle or GraphViz
```

La ressource avait une dépendance "auto-référente", elle surveillait le fichier que la fonction copiait.

### top-scope variable being used without an explicit namespace

http://puppet-lint.com/checks/variable_scope/

Il faut associer les variables à l'espace de nom du module :

$postfix::myvar <-- ok
$myvar <-- nok

La seconde définition fonctionne mais emet un warning avec puppet-lint

$::myvar <-- nok : cette syntaxe passe le lint mais génère une erreur lors de l'executation réel de la classe. 

### Duplicate declaration dans une classe classique



### Function lookup() did not find a value for the name 'value'

la clé n'est pas trouvé, raisons :
1) elle n'est pas définie dans hiera
2) elle est définie dans hiera mais non importé dans le manifest : $whoami = hiera('whoami')
3) l'orthographe diffère
4) ce n'est pas une clef hiera mais un fact


### seule une partie de mes entrées hiera sont prisent en compte 

essayez de faire un *lookup*  avec des stratégie de merge différentes  

Exemple, le merge first s'arrête sur la première clef trouvée

```bash
~# puppet lookup apt::sources  --merge first
---
debian_stable:
  comment: This is the iWeb Debian stable mirror
  location: http://mirrors.online.net/debian
  release: stretch
  repos: main contrib non-free
  key:
    id: A1BD8E9D78F7FE5C3E65D8AF8B48AD6246925553
    server: subkeys.pgp.net
  include:
    src: false
    deb: true
```

alors que le merge deep renverra l'ensemble des clefs

```bash
---
knot:
  comment: This is the iWeb Debian stable mirror
  location: http://mirrors.online.net/debian/knot
  release: stretch
  repos: main contrib non-free
  key:
    id: A1BD8E9D78F7FE5C3E65D8AF8B48AD6246925553
    server: subkeys.pgp.net
  include:
    src: false
    deb: true
debian_stable:
  comment: This is the iWeb Debian stable mirror
  location: http://mirrors.online.net/debian
  release: stretch
  repos: main contrib non-free
  key:
    id: A1BD8E9D78F7FE5C3E65D8AF8B48AD6246925553
    server: subkeys.pgp.net
  include:
    src: false
    deb: true
```

## classes

### ma variable ou mon saut de ligne n'est pas pris en compte

Passer de la simple quote à la double. 
