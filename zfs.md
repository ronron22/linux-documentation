* [obtenir des infos sur un volume] (#obtenir-des-infos-sur-un-volume)

### taille du zpool

```bash
zpool list
NAME    SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
zroot  10.9T  1.43T  9.44T         -     4%    13%  1.00x  ONLINE  -
```

### status du zpool

```bash
zpool status
  pool: zroot
 state: ONLINE
  scan: none requested
config:

	NAME        STATE     READ WRITE CKSUM
	zroot       ONLINE       0     0     0
	  raidz1-0  ONLINE       0     0     0
	    ada0p4  ONLINE       0     0     0
	    ada1p4  ONLINE       0     0     0
	    ada2p4  ONLINE       0     0     0

errors: No known data errors
```

### afficher l'historique des opérations sur le zpool

```bash
zpool history
```

### lister les volumes

```bash
zfs list
```

```bash
zfs list -r monvolume
```

### obtenir des infos sur un volume

Obtenir le type par exemple :

```bash
zfs get type  vmpool/data/vm-104-disk-1 
NAME                       PROPERTY  VALUE   SOURCE
vmpool/data/vm-104-disk-1  type      volume  -
```

Obtenir toutes les infos

```bash
zfs get all  vmpool/data/vm-104-disk-1 
```

### voir les montages 

```bash
zfs mount
```

## snapshots

Les snapshots sont normalement visible à la racine du zpool, dans le répertoire *.zfs*, attention, il ne peut être affiché, juste traversé.

### lister les snapshots

```bash
zfs list -t snapshot
```

### obtenir des infos

```bash
zfs get all monsnap
```

