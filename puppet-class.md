# Puppet et classes

### qu'est-ce qu'une parameterized class

Une parameterized class est classe prenant des paramètres, exemple

```bash
class tororo ($version) {
  Notify{"résultat : $version": }
}
```

La classe est appelée ainsi

```bash
class { totoro:
   version => '1.3',
}
```

Cette appel ce place dans un node, un profile, un rôle etc...
