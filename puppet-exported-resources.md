# Puppet

## Exported resources

Une ressource exportée spécifie un état désiré pour une ressource, il ne gère pas ladite ressource sur le système cible

en gros c'est une déclaration qui sera utilisé par un autre noeud


### Show exported resources

Voir les ressources exportés

puppet query -u "https://puppetdb.com:8081/" --cert=/etc/puppetlabs/puppet/ssl/certs/my.pem --key=/etc/puppetlabs/puppet/ssl/private_keys/my.pem  "resources[certname, type, title] { exported = true }" | less


### Cleaning exported resources

https://ma.ttias.be/puppet-clearing-particular-node-puppetdb-exported-resources/
