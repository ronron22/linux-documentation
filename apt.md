* [le script de post-remove empêche la suppresion d'un paquet] (#le-script-de-post-remove-empêche-la-suppresion-d'un-paquet)

### Forcer la version backport d'un paquet
Le -t correspond à target release
```
apt-get install haproxy -t jessie-backports
```

### Forcer la version d'un paquet
```
apt-get install  php5-mysql=5.5.30-1~dotdeb+7.1
```
### geler la version d'un paquet
Avec elasticsearch
`echo "elasticsearch hold" | dpkg --set-selections`

On vérifie avec un

```bash
dpkg --get-selections elasticsearch
elasticsearch                                   hold
```
### voir la liste des clés chargées
`apt-key list`

### le script de post-remove empêche la suppresion d'un paquet

allez dans /var/lib/dpkg/info/ puis éditer le script de post-install à problème ex 
util-linux.postinst, y insérer un exit 0 en début de fichier.
relancer le remove.

### virer les paquets rc

https://linuxprograms.wordpress.com/2010/05/12/remove-packages-marked-rc/

### figer des paquets

apt-mark hold "nom du paquet"

### récupérer les fichiers de conf d'un paquet

https://askubuntu.com/questions/66533/how-can-i-restore-configuration-files

```bash
dpkg-deb --fsys-tarfile rspamd_1.8.1-1~stretch_amd64.deb | sudo tar x  -C /tmp/
```
puis fouiller dans /tmp/etc pour y trouver son bonheur. 

### ajouter une clé de repo à apt

```bash
wget https://matrix.org/packages/debian/repo-key.asc
apt-key add repo-key.asc
```

### voir les versions proposé d'un paquet

```bash
apt-cache madison  python2.7
```

### pinning sur repository

```bash
cat  /etc/apt/sources.list.d/varnishcache_varnish41.list
deb https://packagecloud.io/varnishcache/varnish41/debian/ stretch main

apt install varnish=4.1.11-1~stretch

cat  /etc/apt/preferences.d/varnish
Package: varnish
Pin: origin "packagecloud.io"
Pin-Priority: 900 
```
