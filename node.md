## pm2

http://pm2.keymetrics.io/docs/usage/quick-start/

### status

ce mettre en utilisateur et lancer pm2 status

### trouver les fichiers de conf des applications

chercher un `server/main.js`

### où trouver d'autre éléments de configuration

ce mettre en utilisateur puis taper `env` pour rechercher d'éventuelle variables.

### aficher les logs applicatifs

```bash
pm2 logs 
```

ou pour une appli en particulier

```bash
pm2 logs 0
```
