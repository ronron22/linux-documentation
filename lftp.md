add 

```bash
set dns:order "inet6 inet"
set ftp:ssl-protect-data true
set ftp:ssl-force true
set ftp:ssl-auth TLS
set ssl:verify-certificate no
```
to /etc/lftp.conf

and

```bash
lftp localhost -u samintftp_test
```

### forcer le passive mode

```bash
lftp -e 'debug 10;set ftp:passive-mode on;  ls; bye; set ftp:ssl-force true; set ssl:verify-certificate no ' -u  export,g290 ftp://ns338-227.eu
```

### one liner de test

```bash
lftp -e "set ftp:passive-mode on; set ftp:ssl-force true; set ssl:verify-certificate no ; lcd /tmp/ ; get logs/apache/lesincollables.com_access.log" -u moimeme,prout ftp://localhost
```


