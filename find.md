# find

## uses case

### trouve les logs non compressés

`find . -regextype posix-egrep -regex ".*\.log(\.[1-9])?$"` 

### supprimmer les fichiers modifiés en 2017

```bash
find . -type f -newermt 20170110 -not -newermt 20180101 -delete
```

### détails sur le mtime

+1 plus vieux d'au moins un jour
-1 pas plus vieux d'un jour  

### exclure des fichiers d'une suppression

```bash
find .  -maxdepth 1 ! -name  "*local*" ! -name "*old" -type f -exec rm {} \;
```

### chercher des fichiers modifiés entre deux date 

```bash
find . -newermt "2019-09-16" ! -newermt "2019-09-24" -name "*.php"
```
