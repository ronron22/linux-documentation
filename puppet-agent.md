# Puppet agent

### afficher la config de l'agent

```bash
puppet agent --configprint all
```

### vérifier que l'agent est disable

vérifier que le fichier suivant existe
 
```bash
/opt/puppetlabs/puppet/cache/state/agent_disabled.lock
```


