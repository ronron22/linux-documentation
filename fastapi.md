
### en root

apt install virtualenv
apt-get install python3-dev
apt-get install python-dev
pip3 install uvicorn
apt-get install libffi-dev

### dans le virtualenv

pip3 install pyjwt
pip3 install uvicorn
pip3 install psycopg2
pip3 install pydantic
pip3 install fastapi
pip3 install sqlalchemy
pip3 install bcrypt
pip3 install python-multipart
