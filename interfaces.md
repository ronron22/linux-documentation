
cat  if-pre-up.d/vip 
#!/bin/bash

if [ "$IFACE" == "eth0:mysqlvip" ] ; then
        if ! ping -W 1 -c 1 8.78.8.12 &> /dev/null ; then
                echo -e "\e[31mtarget unreachable\e[0m" ; exit 2
        fi
fi


cat  interfaces.d/mysqlvip 
auto eth0:mysqlvip
iface eth0:mysqlvip inet static
        address 192.168.3.59
        netmask 255.255.255.0


et ajouter "source /etc/network/interfaces.d/*" à network interface
