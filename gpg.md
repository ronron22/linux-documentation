# gpg

## principe d'utilisation

### comment echanger des messages chiffrés

1. alice envoi sa clef public sur un keyserver
2. bob récupère la clef public d'alice (import)
3. bob signe sa clef avec la clef public d'alice
4. bob chiffre un message en utilisant l'id de la clef d'alice puis lui envoi
5. alice décrypte le message avec sa clef

https://linuxconfig.org/how-to-encrypt-and-decrypt-individual-files-with-gpg

### chiffrement d'un fichier

```bash
gpg -e -r "Your Name" myfile
```

### déchiffrement

```bash
gpg -d myfile
```

### listing des clefs

```bash
gpg --list-secret-keys
```

### envoi de la clef sur les serveurs

```bash
gpg --send-keys FFTTDDD
```

ou

```bash
gpg --keyserver hkp://pgp.mit.edu --send-key 6FF171807484CEF3829
```

### recherche sur le keyserver

```bash
gpg --keyserver hkp://pgp.mit.edu --keyserver-options timeout=2 --search-keys "an@google.fr"
```

### importer les clés

```bash
gpg2 --recv-keys ddHJUY
```

### signer la clef reçu 

```bash
gpg --sign-key 171807484CE
```

### sauvegarde et restauration

https://msol.io/blog/tech/back-up-your-pgp-keys-with-gpg/

### utiliser une configuration alternative

```bash
gpg --homedir ~/Téléchargements/my_gnupg/ -k
```
