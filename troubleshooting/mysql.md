## Percona tools

### pt-diskstats

Obtenir des stats disque 

```bash
 pt-diskstats
  #ts device       rd_s rd_avkb rd_mb_s rd_mrg rd_cnc   rd_rt    wr_s wr_avkb wr_mb_s wr_mrg wr_cnc   wr_rt busy in_prg    io_s  qtime stime
  0.9 nvme1n1       0.0     0.0     0.0     0%    0.0     0.0    13.2     2.6     0.0     8%    0.0     0.6   0%      0    13.2    0.3   0.3
  0.9 nvme1n1p4     0.0     0.0     0.0     0%    0.0     0.0     8.8     3.9     0.0    11%    0.0     0.0   0%      0     8.8    0.0   0.0
  0.9 md4           0.0     0.0     0.0     0%    0.0     0.0     9.9     2.2     0.0     0%    0.0     0.0   0%      0     9.9    0.0   0.0
  0.9 nvme0n1       0.0     0.0     0.0     0%    0.0     0.0    13.2     2.6     0.0     8%    0.0     0.6   0%      0    13.2    0.3   0.3
  0.9 nvme0n1p4     0.0     0.0     0.0     0%    0.0     0.0     8.8     3.9     0.0    11%    0.0     0.0   0%      0     8.8    0.0   0.0
  0.9 dm-4          0.0     0.0     0.0     0%    0.0     0.0     6.6     3.3     0.0     0%    0.0     0.7   0%      0     6.6    0.0   0.7
```

### pt-query-digest

Stats sur les slowlog

L'interessant est surtout le max Exec time, le max Lock time, 550ms dans l'exemple. 

```bash
pt-query-digest /var/log/mysql/mysqld-slow.log
/var/log/mysql/mysqld-slow.log:  46% 00:34 remain
/var/log/mysql/mysqld-slow.log:  94% 00:03 remain

# 63.8s user time, 150ms system time, 49.28M rss, 110.98M vsz
# Current date: Tue Jul 16 17:44:28 2019
# Hostname: ns
# Files: /var/log/mysql/mysqld-slow.log
# Overall: 395.40k total, 122 unique, 0.41 QPS, 0.00x concurrency ________
# Time range: 2019-07-05T09:34:01 to 2019-07-16T15:44:27
# Attribute          total     min     max     avg     95%  stddev  median
# ============     ======= ======= ======= ======= ======= ======= =======
# Exec time           308s    19us   554ms   779us     2ms     3ms   152us
# Lock time            30s     8us   534ms    76us   204us     2ms    44us
# Rows sent         81.41M       0   1.39k  215.89   1.33k  459.04    2.90
# Rows examine     227.75M       0  30.48k  603.99   3.19k   1.47k   13.83
# Query size       128.21M      18   2.51k  340.01   1.46k  514.40  112.70

# Profile
# Rank Query ID           Response time Calls  R/Call V/M   Item
# ==== ================== ============= ====== ====== ===== ==============
#    1 0x73F75B031E912996 99.9386 32.4%  41592 0.0024  0.00 SELECT be_options
#    2 0x7513CDCC36BFC3AF 71.6532 23.2%   3218 0.0223  0.00 SELECT be_blc_links be_blc_instances
#    3 0x9209DB42C938139C 24.2492  7.9%  35177 0.0007  0.00 SELECT UNION catalog_category_entity_varchar v_entity_attribute catalog_category
_entity_int v_entity_attribute catalog_category_entity_text v_entity_attribute catalog_category_entity_datetime v_entity_attribute catal
og_category_entity_decimal v_entity_attribute
#    4 0x1E729386A335630C 22.4421  7.3%  18860 0.0012  0.00 SELECT core_config_data
#    5 0x4082873A43FD906B 13.6529  4.4%  13053 0.0010  0.00 DELETE e_useronline
#    6 0xD568B8AF7B0597CD 11.0687  3.6%   2755 0.0040  0.00 SELECT catalogsearch_query
#    7 0x2448FF3777FF52A7  5.8395  1.9%   6081 0.0010  0.00 SELECT v_attribute catalog_v_attribute v_entity_attribute v_attribute_labe
l
#    8 0x0DD143F1988C1366  4.7342  1.5%   8423 0.0006  0.00 SELECT UNION tax_calculation tax_calculation_rule tax_calculation_rate tax_calcula
tion_rate_title tax_calculation tax_calculation_rule tax_calculation_rate tax_calculation_rate_title
#    9 0x8E26F2DD62A180AF  4.2796  1.4%   3053 0.0014  0.00 SELECT catalog_category_entity catalog_category_entity_varchar catalog_category_en
tity_int
#   10 0xE6CB0CE1DF430230  3.9350  1.3%  18791 0.0002  0.00 SELECT core_translate
#   11 0xE1C46ECD6C82D1CF  3.8188  1.2%  25515 0.0001  0.00 SELECT review_entity_summary
#   12 0x96DAD12134495602  3.5554  1.2%   3383 0.0011  0.00 SELECT catalog_category_entity catalog_category_entity_varchar
#   13 0x5BE1D82D121F9635  3.0145  1.0%  18858 0.0002  0.00 SELECT core_store
#   14 0x04248D594D4723A6  2.9984  1.0%     14 0.2142  0.16 SELECT information_schema.tables
#   15 0x1FFB67DDD5F32711  2.4178  0.8%  18860 0.0001  0.00 SELECT core_resource
#   16 0x61E9A53653C8AD00  2.1845  0.7%   2705 0.0008  0.00 SELECT v_attribute catalog_v_attribute v_entity_attribute v_attribute_labe
l
#   17 0x8D2C09B366F1F64D  2.1399  0.7%  18860 0.0001  0.00 SELECT core_website
#   18 0x551C7FAB1F44BB67  2.1146  0.7%   4774 0.0004  0.00 SELECT v_attribute catalog_v_attribute v_attribute_label
#   19 0x158122EF7ADC6D9F  2.0638  0.7%  23702 0.0001  0.00 SELECT v_attribute_set
#   20 0x94C5CD4B0150ED98  1.7569  0.6%   4660 0.0004  0.00 SELECT catalin_seo_attribute_url_key
# MISC 0xMISC             20.5077  6.7% 123063 0.0002   0.0 <102 ITEMS>

# Query 1: 0.04 QPS, 0.00x concurrency, ID 0x73F75B031E912996 at byte 0 __
# This item is included in the report because it matches --limit.
# Scores: V/M = 0.00
# Time range: 2019-07-05T09:34:01 to 2019-07-16T15:44:27
# Attribute    pct   total     min     max     avg     95%  stddev  median
# ============ === ======= ======= ======= ======= ======= ======= =======
# Count         10   41592
# Exec time     32    100s     1ms    18ms     2ms     3ms   340us     2ms
# Lock time      7      2s    18us     5ms    54us    69us    34us    54us
# Rows sent     66  54.46M     448   1.39k   1.34k   1.33k  180.18   1.33k
# Rows examine  56 128.69M   1.51k   3.24k   3.17k   3.19k  335.22   3.19k
# Query size     2   2.90M      73      73      73      73       0      73
# String:
# Databases    prod_mumto... (37591/90%)... 2 more
# Hosts        localhost
# Users        prod_mumto... (37591/90%)... 2 more
# Query_time distribution
```

### pt-deadlock-logger

Pour logguer des deadlock.
pas vu en action

### pt-summary

Pour avoir un super résumé avec les principals métriques.



