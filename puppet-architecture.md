# Puppet architecture

## Profile, rôle et Hiera ou comment dissocier le code des données

L'idée la suivante, Hiera n'hébergerait que les variables et les appels aux rôles. Les appels aux classes (code) serait hérités des profiles eux-même appelés par les rôles. 

De plus, on peut, à partir de hiera, appeller un role, un profile ou une classe de module, cela nous offre une granularité.


Exemple :

### Avant

Tout est dans hiera, l'appel des classes et les variables.

```bash
~# cat server1.yaml
---
class:
  - apache
...
```

```bash
~# cat isweb.yaml
---
apache::enable: true
apache:vhosts_list:
	- toto.com
	- titi.com
	- tagada.com
...
```
 et c'est tout

### Après

plus compliqué mais plus souple et clair, on mixe hiera, les profiles et les roles. Hiera n'hébergeant pratiquement plus que les variables et les appels aux rôles. 

On appelle le role Puppet

```bash
~# cat mutu.yaml
---
class:
  - role::mutu
...
```

On ne touche pas aux rôles hiera (rôles correspondant aux facts et nom les rôles *modules*)

```bash
~# cat iswebserver.yaml
---
apache::enable: true
apache:vhosts_list:
	- toto.com
	- titi.com
	- tagada.com
...
```
On créé un rôle **web** appellant le profile **apache**


```bash
~# cat site/role/manifests/web.pp
class role::web inherits base {
	include profile::apache
...
}
```

On créé un profile **apache** incluant le module apache

```bash
~# cat site/profile/manifests/apache.pp
class profile::apache inherits profile {
	include apache
...
}
```

## Cas réel

Ma société s'appelle zonama, elle vend des produits en ligne tel des livres, des films etcetera..

### Définition des stacks

Maintenant imaginons quelle mette en oeuvre les trois types de stacks de serveur suivantes :

1. haproxy, varnish, apache, memcache et php-fpm  
Cette stack s'appelle **zonamaweb**

2. mysql  
Cette stack s'appelle **zonamasql**

3. solr  
Cette stack s'appelle **zonamaslr**

Répartis sur les environnements suivants :

### Définition des environnements 

* production alias **prd**
* préproduction alias **ppd**
* recette alias **rct**

Chaque environnement aura des valeurs de configuration différentes même s'ils font appelles aux mêmes composants.  

### Nommage serveur

Le nom complet d'un de mes serveurs web de production sera donc **zonamaprdweb3**, celui d'un serveur mysql de préprod sera **zonamappdsql2** et enfin celui d'un serveur solr de recette sera **zonamaarctslr4** 

### Voici comment nous  gérerions cette architecture avec Puppet.

Nous aurons aussi trois environnements :

* production
* préproduction
* recette

```bash
ls /etc/puppetlabs/code/environments/
production  preproduction recette
```

Nous aurons aussi trois rôles puppet :

* zonamaweb
* zonamasql
* zonamaslr

### Composition des profiles et relations avec les classes 

et enfin nous définirons les profiles suivants :

* haproxy1  
  appellant les classes :  
  * haproxy
  * backup haproxy
  * supervision haproxy
* varnish1  
  appellant les classes :  
  * varnish
  * backup varnish
  * supervision varnish
* apache1  
  appellant les classes :  
  * apache
  * backup apache
  * supervision apache
* php-fpm1  
  appellant les classes :  
  * php-fpm
  * backup php-fpm
  * supervision php-fpm
* memcached1  
  appellant les classes :  
  * memcached
  * backup memcached
  * supervision memcached
* mysql1  
  appellant les classes :  
  * mysql
  * backup mysql
  * supervision mysql
* solr1  
  appellant les classes :  
  * solr
  * backup solr
  * supervision solr
* base1  
  appellant les classes :  
  * base
  * agent de sauvegarde
  * agent de supervision


### Relation entre rôles et profiles 

Le rôle zonamaweb appellera les profiles

* base
* haproxy
* varnish
* apache 
* php-fpm
* memcached

Le rôle zonamasql appellera les profiles

* base
* sql

Le rôle zonamaslr appellera les profiles

* base
* solr

### Côté hiera

Nous créerons les externals facts suivants :

* production
* preproduction
* recette
* isweb
* issql
* issolr

La hierarchie Hiera ressemblant à ça (exemple avec l'en de production) :

```bash
  - name: "Per-node data (yaml version)"
    path: "nodes/%{::trusted.certname}.yaml"

  - name: "Per codename"
    path: "os/distro/%{facts.os.distro.codename}.yaml"

  - name: "Per os family"
    path: "os/family/%{facts.os.family}.yaml"

  - name: "Per environnement"
    path: "env/environnement.yaml"
    #  héberge les valeurs spécifique à l'environnement
    # quelque soit le service

  - name: "Per role"
    path: "roles/%{::role}.yaml"
```

### Associons dynamiquement les rôles avec les hosts via les externals facts

l'idée est la suivante, plutôt que d'associer via hieradata/nodes/ un noeud avec un rôle, on défini via un fact présent sur le noeud, le rôle qui échoiera à celui-çi. 

1. cela implique de précréer les rôles dans hieradata
2. cela implique de précréer les rôles dans site/role/
3. actuellement, cela nous contraint à ce qu'un noeud ne puisse avoir plus d'un rôle

Mon organisation s'appelle ronronland

créons un répertoire dédié à mon organisation

```bash
mkdir /etc/ronronland
```

lions le répertoire des externals facts avec notre organisation

```bash
ln -s /opt/puppetlabs/facter/facts.d/ /etc/architux/
```

poussons notre rôle (mutu):

```bash
echo -e "---\nrole: mutu1" > /etc/architux/facts.d/role.yaml
```

Ajout dans hiera.yaml (plus reload master)

```bash
echo -e "---\nrole: mutu1" > /etc/architux/facts.d/role.yaml
  - name: "Per role"
    path: "roles/%{::role}.yaml"
```

ajout dans hieradata de la prise en compte du facts roles

```bash
~# cat hieradata/roles/mutu.yaml
---
class:
  - role::mutu
```
