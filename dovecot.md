## Avoir des stats sur les connexions en cours 

```
doveadm who -a  /var/run/dovecot/anvil | less
```

### test authentication

```
doveadm auth test -x service=imap   info@studio.com
```

### lister les boites d'un compte

```
doveadm list -u test@ecole.org
```

### supprimer les spams

```
doveadm expunge -u test@ecole.org mailbox INBOX.toto all
```

```
for i in $(plesk bin mail -l | tr '\t' ' ' | cut -d' ' -f 3- | grep ecole.org); do doveadm expunge -u "$i" mailbox INBOX.Spam before 30d ; done
```
