### Obtenir le status d'un module 

```bash
php-fpm-cli -connect 127.0.0.1:9000 -r 'print_r(opcache_get_status(false));'
```

### calculer la mémoire utiliser par les process php

https://stackoverflow.com/questions/1072975/how-can-i-monitor-memory-usage-of-php-in-linux

```bash
total=0; for i in `ps -C php-cgi -o rss=`; do total=$(($total+$i)); done; echo "Memory usage: $total kb";
```

### vérifier si php tourne en fpm ou en module apache

```bash
<?php
    echo getmypid() . "</br>";
?>
```
