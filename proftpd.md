# proftpd

### limiter les accès 

#
# Simple .ftpaccess file to control which IPs
# can access this directory structure
#
<Limit>
	Allow 212.32.5.0/26
	Allow 158.152.0.0/16
	DenyAll
</Limit>
#
# end
#
