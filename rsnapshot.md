# 

compréhension sur zfs

le 22/07 on supprime le snapshot du 18/06 et on en recréé un

/usr/local/bin/rsnapshot -v -t  -c /etc/rsnapshot/02247.gs.ovh.net.conf daily
echo 62496 > \
    /var/run/rsnapshot/rsnapshot-02247.gs.ovh.net.pid
/usr/bin/ssh 02247.gs.ovh.net '/usr/local/bin/rsnapshot-exec \
    2>&1' >> /var/log/rsnapshot/rsnapshot-02247.gs.ovh.net.log
/usr/local/bin/rsnapshot.zfs.cmd_rm -rf \
    /bkp/ff102247/02247.gs.ovh.net/daily.6/
mv /bkp/ff102247/02247.gs.ovh.net/daily.5/ \
    /bkp/ff102247/02247.gs.ovh.net/daily.6/
mv /bkp/ff102247/02247.gs.ovh.net/daily.4/ \
    /bkp/ff102247/02247.gs.ovh.net/daily.5/
mv /bkp/ff102247/02247.gs.ovh.net/daily.3/ \
    /bkp/ff102247/02247.gs.ovh.net/daily.4/
mv /bkp/ff102247/02247.gs.ovh.net/daily.2/ \
    /bkp/ff102247/02247.gs.ovh.net/daily.3/
mv /bkp/ff102247/02247.gs.ovh.net/daily.1/ \
    /bkp/ff102247/02247.gs.ovh.net/daily.2/
/usr/local/bin/rsnapshot.zfs.cmd_cp -al \
    /bkp/ff102247/02247.gs.ovh.net/daily.0 \
    /bkp/ff102247/02247.gs.ovh.net/daily.1
/usr/local/bin/rsync-no-vanished -a --stats --numeric-ids --delete-excluded \
    --log-file=/var/log/rsnapshot/rsync-02247.gs.ovh.net.log \
    --exclude=/dev --exclude=/proc --exclude=/sys --exclude=/tmp \
    --exclude=/mnt --exclude=/var/log/apache2 --exclude=/var/log/httpd \
    --exclude=/var/named/chroot --exclude=/var/lib/php/session/ \
    --exclude=/var/lib/php5/ --exclude=/var/amavis/tmp \
    --exclude=/var/lib/amavis/tmp --exclude=/var/spool/amavisd/tmp \
    --exclude=/var/spool/postfix --exclude=/var/lib/mysql/*relay-bin* \
    --exclude=/home/ff102247/nas --exclude=/home/ff102247/logs \
    --rsh=/usr/bin/ssh -o StrictHostKeyChecking=no \
    root@02247.gs.ovh.net:/ \
    /bkp/ff102247/02247.gs.ovh.net/daily.0/./
touch /bkp/ff102247/02247.gs.ovh.net/daily.0/
/usr/bin/ssh 02247.gs.ovh.net '/usr/local/bin/rsnapshot-exec \
    postexec 2>&1' >> \
    /var/log/rsnapshot/rsnapshot-02247.gs.ovh.net.log; \
    /usr/bin/touch \
    /var/log/rsnapshot.done/rsnapshot-02247.gs.ovh.net.done


