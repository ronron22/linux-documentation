# csync

## Ressources

* https://linuxaria.com/howto/csync2-a-filesystem-syncronization-tool-for-linux
* https://www.csync.org/userguide/
* https://build.opensuse.org/package/view_file/openSUSE:Factory/csync2/csync2-README.quickstart

### passer en verbeux

multiplier les -v tel que -vvv


### nettoyage bulldozer

En cas d'erreur de certificat, d'ip ou de contenu: vider /var/lib/csync2.

Car csync2 enregistre sa conf dans son backend..

### fichier marqué comme dirty

sur le master

 csync2 -vvu
