# Icinga

https://icinga.com/docs/icinga2/latest/doc/15-troubleshooting/

### tester la config

```bash
icinga2 daemon -C
```

### voir la définition des services

```bash
icinga2 object list 
```
