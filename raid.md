# Le raid

## Ressources

https://www.linuxtricks.fr/wiki/mdadm-raid-logiciel-sous-linux

### obtenir le status et des infos

```bash
cat /proc/mdstat
Personalities : [raid1] [raid0] [raid6] [raid5] [raid4]
md4 : active raid1 sda4[0] sdb4[1]
      3895724992 blocks [2/2] [UU]
      bitmap: 1/30 pages [4KB], 65536KB chunk

md2 : active raid1 sda2[0] sdb2[1]
      10238912 blocks [2/2] [UU]

unused devices: <none>
```

ou avec mdadm

```bash
mdadm --detail /dev/md2
/dev/md2:
        Version : 0.90
  Creation Time : Tue Jun  4 13:48:31 2019
     Raid Level : raid1
     Array Size : 10238912 (9.76 GiB 10.48 GB)
  Used Dev Size : 10238912 (9.76 GiB 10.48 GB)
   Raid Devices : 2
  Total Devices : 2
Preferred Minor : 2
    Persistence : Superblock is persistent

    Update Time : Wed Jun  5 15:09:38 2019
          State : clean
 Active Devices : 2
Working Devices : 2
 Failed Devices : 0
  Spare Devices : 0

           UUID : f136cf62:bbdc945f:a4d2adc2:26fd5302
         Events : 0.3

    Number   Major   Minor   RaidDevice State
       0       8        2        0      active sync   /dev/sda2
       1       8       18        1      active sync   /dev/sdb2
```

### installer sgdisk

apt-get install gdisk

### copie de partition

Atention :

sgdisk -R=/dev/sddestination /dev/sdacopier

### ajout d'une partition au raid

mdadm --manage /dev/md1 -a /dev/sdb
