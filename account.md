### verouiller un compte

```bash
passwd -l xy54529035
```

### vérifier

```bash
passwd -S xy54529035
```

### déverouiller un compte

```bash
passwd -u xy54529035
```

### ajouter un compte à un groupe

```bash
gpasswd -d adminxy54529035 sudo
```
