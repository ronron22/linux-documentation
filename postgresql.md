## Dump et restore

### Dump des privilèges

```bash
su - postgres
pg_dumpall -g > /tmp/globals_only.dump
```

### Insertion

```bash
su - postgres
cat /tmp/globals_only.dump | psql
```

### Dump d'une base 

```bash
su - postgres
pg_dump mabase > /tmp/mabase.sql
```

### Réinsertion

```bash
su - postgres
psql mabase < /tmp/mabase.sql
```

## Création des comptes

### création de l'utilisateur ainsi que de son mot de passe

```bash
CREATE USER tagada WITH PASSWORD 'pouet';
```

### lister les utilisateurs ainsi que leurs privilèges

```bash
\du
```

### création de la base

```bash
CREATE DATABASE rantanplan OWNER tagada;
```

# commandes usuelles

### lister les bases

```bash
\l
```

### ce connecter à une base

```bash
\c mabase
```

### lister les tables

```bash
\dt
```

## troubleshooting

### ERROR:  relation "" does not exist postgres

```bash
GRANT usage on schema public to latbune_php7;
```
### Insufficient privilege: 7 ERROR:  permission denied for relation permalinks

```bash
GRANT ALL  ON database latbune_php7 TO latbune_php7;
```

```bash
SELECT * FROM pg_roles ;
```

Vérifier avec un "\connect" et un "\dt" que le propriétaire est bien celui qui doit être.

```bash
                     Liste des relations
 Schéma |             Nom             | Type  | Propriétaire
--------+-----------------------------+-------+---------------
 public | allowed_block          | table | tagada
 public | applications                | table | tagada
```

puis changement du owner des tables 

Bien vérifier que l'on attaque les bonnes tables avec un 

```bash
while read line ; do psql latbune_php7 -c "select * from  $line limit 1;" ; done <  /tmp/out2
```

```bash
while read line ; do psql tagada -c "alter table $line owner to tralala;" ; done <  /tmp/out2
```

### affichage à la mysql \G

taper 

```bash
\x
```

avant de lister une table

### un describe 

```bash
\d+ 'ma table'
```
