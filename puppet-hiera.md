# Hiera

## configuration hiera (hiera.yaml)

**Tous changement dans ce fichier doit être suivi d'un rechargement Puppet**

#### comment interroger hiera5

```bash 
puppet lookup "variable"
```

Ou **class::var**

```bash 
puppet lookup ntp::service_name
```

#### comment appeller une variable d'un espace de nom différent

exemple, je veux utiliser 

```bash 
knot::service::service_name: 'knot'
```

dans la classe config, normalement je ne peux pas à moins d'ajouter la variable suivante

```bash 
knot::config::service_name: 'knot'
```

mais cette solution à un gout amer.. 

En voici une autre, dans la classe, appelez l'espace de nom complet

```bash 
$service_name   = hiera('knot::service::service_name'),
```

### comment pousser les variables dans hiera pour un module qui ne s'y prête pas

Pré-requis, ne pas toucher au module qui est géré par r10k

Dans l'exemple que je vais décrire, je pousserais les variables dans profile/manifest/

Il faut installer le module postgresql de Puppetlabs

puis définir un profile dans ce genre

```bash 
class profile::postgresql {

    $listen_addresses = hiera('profile::postgresql::listen_addresses')

  class { 'postgresql::server':

    listen_addresses => $listen_addresses,

  }

}
```

Et l'entrée hiera coresspondante doit ressembler à cela

```bash 
---
profile::postgresql::listen_addresses:  '127.0.0.2'
...
```

### seule une partie de mes entrées hiera sont prisent en compte 

essayez de faire un *lookup*  avec des stratégie de merge différentes  

Exemple, le merge first s'arrête sur la première clef trouvée

```bash
~# puppet lookup apt::sources  --merge first
---
debian_stable:
  comment: This is the iWeb Debian stable mirror
  location: http://mirrors.online.net/debian
  release: stretch
  repos: main contrib non-free
  key:
    id: A1BD8E9D78F7FE5C3E65D8AF8B48AD6246925553
    server: subkeys.pgp.net
  include:
    src: false
    deb: true
```

alors que le merge deep renverra l'ensemble des clefs

```bash
---
knot:
  comment: This is the iWeb kknot Debian stable mirror
  location: http://mirrors.online.net/debian/knot
  release: stretch
  repos: main contrib non-free
  key:
    id: A1BD8E9D78F7FE5C3E65D8AF8B48AD6246925553
    server: subkeys.pgp.net
  include:
    src: false
    deb: true
debian_stable:
  comment: This is the iWeb Debian stable mirror
  location: http://mirrors.online.net/debian
  release: stretch
  repos: main contrib non-free
  key:
    id: A1BD8E9D78F7FE5C3E65D8AF8B48AD6246925553
    server: subkeys.pgp.net
  include:
    src: false
    deb: true
```

#### mais par défaut, le lookup est toujours unique ?

hum, hum

Ajouter un *lookup.yaml* dans *hieradata*

```bash
cat hiera.yaml
...
  - name: "Other YAML hierarchy levels"
    path: "common.yaml"

  - name: "lookup strategy for hiera merging"  
    path: "lookup.yaml"
```

Et voici le fichier *lookup.yaml*

```bash
cat hieradata/lookup.yaml 
lookup_options:
  augeas:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  classes:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
  files:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  templates:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  packages:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  users:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  users_realize:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  authorized_keys:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  execs:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  file_lines:
    merge:
      strategy: deep
      knockout_prefix: "--"
      sort_merged_arrays: true
      merge_hash_arrays: true
  apt::sources:
    merge:
      strategy: deep
      knockout_prefix: "--"
  apt::confs:
    merge:
      strategy: deep
      knockout_prefix: "--"
  apt::pins:
    merge:
      strategy: deep
      knockout_prefix: "--"
  apt::settings:
    merge:
      strategy: deep
      knockout_prefix: "--"
  apt::ppas:
    merge:
      strategy: deep
      knockout_prefix: "--"
  apt::keys:
    merge:
      strategy: deep
      knockout_prefix: "--"
  knot::install:
    merge:
      strategy: deep
      knockout_prefix: "--"
  knot::config:
    merge:
      strategy: deep
      knockout_prefix: "--"
```

Vous aurez ainsi, une *strategy* de *merge deep* pour les entrées présentes dans *lookup*.

#### problème, mon entrée dans le hiera local de mon module n'est pas prise en compte par le merge deep

...je n'ai pas de réponse propre, juste un contournement crade qui consiste à pousser le hash dans le params.pp du module..

exemple, je veux ajouter un dépôts apt, mais je veux le faire à partir du module *knot* et non *apt* ou dans le *hiera* principal.. 

```bash
 cat modules/knot/manifests/params.pp 
class knot::params {

  case $::osfamily {
    'Debian': {
      apt::source { 'knot':
        location => 'https://deb.knot-dns.cz/knot/',
        repos    => 'main',
      }
    }
  }

}
```

et importer celui via l'init.pp

### hiera, lookup et knockout

knockout_prefix sert à spécifier le préfixe qui permettra d'outrepasser une ligne spéécifique

exemple :

knockout_prefix: '--'

ne tiendra pas compte de la clef *toto* comme dans l'exemple suivant :

```bash
classes:
  - tata
  - --toto
  - titi
```

le résultat du *lookup* devrait ressembler à cela

```bash
[''tata', 'titi']
```

### troubleshooter une requête de lookup

verbose

```bash
puppet lookup apt::sources --merge deep --explain
```

ultra verbose

```bash
puppet lookup apt::sources --merge deep --debug
```
