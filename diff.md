###  obtenir un diff clean

```bash
diff --suppress-common-lines file1.txt file2.txt
```

### supprimmer les différences non significatives

```bash
diff -E -b  --suppress-blank-empty
```

### comparer deux arborescences

```bash
diff -aNur  misronron/stage/ ronron2046/misronron/stage/ |  colordiff | less -R
```
