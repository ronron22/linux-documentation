# plesk

## exploitation

### upgrade on command line

```bash
plesk installer --select-product-id plesk --select-release-current --upgrade-installed-components --include-components-from-class vendor=parallels --include-components-from-class patched
```

## troubleshooting

### unable to find psa-health-monitor

Go on their repository and install the package manualy

```bash
cd /tmp/
wget http://ch.origin.autoinstall.plesk.com/PSA_17.8.1
1/dist-deb-Debian-9.0-x86_64/opt/hmonitor/psa-health-monitor_17.8.11-debian9.0.build1708180212.17_all.deb
dpkg -i psa-health-monitor_17.8.11-debian9.0.build1708180212.17_all.deb
```

### admin doesn't respond

```bash
curl -I https://mypleskserver:8443
```

```bash
/etc/init.d/sw-engine restart
```

## MySQL

utiliser "plesk db" pour ouvrir un  prompte mysql

### voir les logs

```bash
 plesk log
access_log  --all       error_log   maillog     panel.log
```

### Où se situent les logs sur plesk ?

https://support.plesk.com/hc/en-us/articles/213403509-Plesk-for-Linux-services-logs-and-configuration-files-#roundcube

### Lister les utilisateurs mails et leurs mdp

```bash
/usr/local/psa/admin/sbin/mail_auth_view
```

### panel.ini

https://support.plesk.com/hc/en-us/articles/213914565-Invalid-nginx-configuration-nginx-emerg-client-max-body-size-directive-is-duplicate
