# corosync

## ressources

https://clusterlabs.org/pacemaker/doc/en-US/Pacemaker/1.1/html/Pacemaker_Explained/s-failure-handling.html

http://eole.ac-dijon.fr/documentations/2.4/completes/HTML/ModuleSphynx/co/94-commandes_crm.html

## exploitation

### voir le status

```bash
crm status
```

### Voir la configuration en cli

```bash
/usr/sbin/crm configure show
```

### reset the counter of crm_mon -f

```bash
crm_resource --cleanup -r mysql-private
crm_resource --cleanup -r redis-private
```
