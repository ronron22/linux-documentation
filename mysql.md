#  MySQL

## upgrade 5.6 vers 5.7

erreur lors du dump de l'information_schema

```bash
mysqldump: Couldn't execute 'SELECT /*!40001 SQL_NO_CACHE */ * FROM `GLOBAL_STATUS`': The 'INFORMATION_SCHEMA.GLOBAL_STATUS' feature is disabled; see the documentation for 'show_compatibility_56' (3167)
```

```bash
set global show_compatibility_56 = on ;
```

̀``bash
cat /etc/mysql/conf.d/nv.cnf
[mysqld]
show_compatibility_56 = on
```

### debug

̀``bash
mysqld --verbose
̀``

### repli en openliner

```bash
mysql -e "stop slave; FLUSH TABLES WITH READ LOCK; show master status; system lvcreate -L1G -s -n mysnap /dev/mapper/vg-liblv ; unlock tables; "
```

### voir les moteurs


```bash
SELECT table_name, table_schema  FROM information_schema.tables  WHERE engine = 'InnoDB';

SELECT table_name, table_schema  FROM information_schema.tables  WHERE engine = 'MyISAM';
```

### régler

Lancer le tuning primer et suivre les reco

### je modifie la conf de open_cache_file mais celui ne bouge pas après un reload 

faire un

```bash
systemctl edit mysql
```

et ajouter

```bash
[Service]
LimitNOFILE=65751
```

### autoriser la connexion root sans mot de passe

Vérifier que le plugin auth_socket est installé 

```bash
mysql -e "show plugins;" | grep auth_socket
auth_socket	ACTIVE	AUTHENTICATION	auth_socket.so	GPL
```

```bash
plugin-load-add=auth_socket.so
auth_socket=FORCE_PLUS_PERMANENT
```
 
```mysql
CREATE USER 'valerie'@'localhost' IDENTIFIED WITH auth_socket;
```

### activer le cache des requêtes


Ajouter 

mysql 5.7
query_cache_type        = 1

### logger les requêtes

SET global general_log = 1;

select * from mysql.general_log ;

#### backup et restauration en réseau

https://gist.github.com/vanjos/6053606

### taille de table 

SELECT
    table_name AS `Table`,
    round(((data_length + index_length) / 1024 / 1024), 2) `Size in MB`
FROM information_schema.TABLES
WHERE table_schema = "back_utf8"
    AND table_name = "jsessions";

### créer un agent de monitoring

CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT, REPLICATION CLIENT, SHOW DATABASES, SUPER, PROCESS ON *.* TO  'readuzr'@'localhost' ;
