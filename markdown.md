### faire un saut de ligne sans faire de paragraphe

il faut terminer la ligne par deux espaces

### faire un ToC

https://www.ask-sheldon.com/create/

add 

```bash
<!-- START doctoc -->
<!-- END doctoc -->
```

on the top of your markdown file then, launch doctop cli, exeample :

```bash
doctoc ~/Bureau/Linux-documentation/troubleshooting/lsof.md --gitlab
```

### mardown vers pdf

```bash
pandoc   -s -o courrier-11-03-2019.pdf courrier-11-03-2019.md
```
