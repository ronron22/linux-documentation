# Puppet params.pp

## Présentation

params est une pseudo classe ne servant qu'à stocker les variables, cela évite de ventiler les variables dans les différents manifestes. 

Les autres classes doivent en hériter explicitement.
