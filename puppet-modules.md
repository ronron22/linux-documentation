# Modules

## pdk 

### création d'un module

```puppet
pdk new module "nom du module"
```

### création d'une classe dans le module

```puppet
pdk new class "nom de ma classe"
```

où

```puppet
pdk new class "nom de ma classe::nom de ma sous-classe"
```

### validation du module

```puppet
pdk validate
```

### test unitaire du module

```puppet
pdk test unit
```


### Hiera dans les modules

1. créer un fichier *hiera.yaml* pour définir la hierachie
2. créer un dossier *data/*
3. dans ce dossier créer les entrées de *hiera*
4. modifier le fichier *metadata.json*  

### hiera, les variables  et modules

#### retirer l'espace de nom du module 

avant

```yaml
nom-du-module::config:clé: 'valeur'
```

après

```yaml
nom-du-module::clé: 'valeur'
```
  
Pour déclarer des clés dans hiera sans espace de nom spécfique, il faut définir les variables dans la class principal présente dans l'init.pp du module

N'oubliez pas de mettre des inherits dans les autres classes.. 

### comment appeler les classes d'un module

avec *include*

include mon_module

avec *class*; cette méthode offrant la possibilitée de surcharger les paramètres

```puppet
class { 'mon_module':
  configuration_directory => '/etc/mon_module',
}
```

ou de changer l'ordre d'execution des classes, par exemple

```puppet
  anchor { 'postfix::begin': } ->
  class { '::postfix::install': } ->
  class { '::postfix::config': } ~>
  class { '::postfix::service': } ->
  anchor { 'postfix::end': }
```

### paramètres optionnels

```puppet
Optional[String] $tagada = undef,
```

Si $tagada est défini (dans le common.yaml par exemple), il est positionné, sinon rien.

Voir le *= undef* comme une valeur par défaut.

#### problème, mon entrée dans le hiera local de mon module n'est pas prise en compte par le merge deep

...je n'ai pas de réponse propre, juste un contournement crade qui consiste à pousser le hash dans le params.pp du module..

exemple, je veux ajouter un dépôts apt, mais je veux le faire à partir du module *knot* et non *apt* ou dans le *hiera* principal.. 

```bash
 cat modules/knot/manifests/params.pp 
class knot::params {

  case $::osfamily {
    'Debian': {
      apt::source { 'knot':
        location => 'https://deb.knot-dns.cz/knot/',
        repos    => 'main',
      }
    }
  }

}
```

et importer celui via l'init.pp
