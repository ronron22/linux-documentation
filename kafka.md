# kafka

### lire un topic 

kafkacat  -b node1.kafka.dolos.digital-network.net -t  indus-servers-queue -e

### créer un topic 

bin/kafka-topics.sh --create  --zookeeper localhost:2181  --replication-factor 1 --partitions 1 --topic ouistiti-gid-queue

## lister les topic

bin/kafka-topics.sh --list   --zookeeper localhost:2181

## obtenir le détail d'un topic

bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic ouistiti-server-queue

### envoyer un message 

bin/kafka-console-producer.sh --broker-list localhost:9092 --topic ouistiti-server-queue

### consomer un message

 bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic ouistiti-server-queue --from-beginning

### vider une queue

bin/kafka-topics.sh --zookeeper 127.0.0.1:2181 --delete --topic ouistiti-main-queue
