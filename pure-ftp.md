### modifier les chemins

le faire à la main puis regénerer avec 

```bash
 pure-pw mkdb
```

puis vérifier avec

```bash
pure-pw list
```

### voir le détail d'un compte

pure-pw show 'nom du compte'

### créer un compte

```bash
pure-pw useradd ftp_test -u 1000 -g 100  -d  /home/s1201/htdocs/test.com/ftp
```

puis

```bash
 pure-pw mkdb
```
