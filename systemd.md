# systemd

## doc

https://www.freedesktop.org/software/systemd/man/systemd.unit.html

### après avoir modifié un service systemd

`systemctl daemon-reload`

### /etc/systemd vs /usr/lib/systemd

* /etc/systemd accueil les services personnalisés

* /lib/systemd accueil les services standard 

### voir une "unit"

```bash
systemctl cat "unit"
```

### personnaliser une "unit" 

```bash
systemctl edit "unit"
```

puis ajouter ses éléments

Ce fichier apparaitra dans **/etc/systemd/system/"unit.d"/override.conf**


## Le réseau

### Le dns

```bash
cat /etc/systemd/resolved.conf 
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See resolved.conf(5) for details

[Resolve]
DNS=209.244.0.3
#FallbackDNS=
#Domains=
#LLMNR=no
#MulticastDNS=no
#DNSSEC=no
#Cache=yes
#DNSStubListener=yes
```

```bash
systemd-resolve --status
```

```bash
service systemd-resolved stop or start
```

### Debian, le réseau et systemd

systemctl status systemd-networkd.service

### créer un mount bind

créer une unit dont le nom correspond au chemin de montage dont les slashe ont été remplacés par des tirets. (ne mettre que le chemin cible, pas le source). 

```bash
cat /etc/systemd/system/home-tagada.mount
[Unit]
Description=Additional sphinx
DefaultDependencies=no
Conflicts=umount.target
Before=local-fs.target umount.target

[Mount]
What=/home/gruik
Where=/home/tagada
Type=none
Options=bind

[Install]
WantedBy=local-fs.target
```

```bash
systemctl enable home-tagada.mount
systemctl daemon-reload
systemctl start home-tagada.mount
```
En cas d'erreur *Where= setting doesn't match unit name. Refusing* 

vérifier que le chemin du montage est bien défini. 

### lister les interfaces réseaux avec systemd

```bash
 networkctl list
IDX LINK             TYPE               OPERATIONAL SETUP
  1 lo               loopback           carrier     unmanaged
  2 eno1             ether              routable    configured
  3 eno2             ether              routable    configured
```

```bash
 networkctl status 2
● 2: eno1
       Link File: /etc/systemd/network/50-public-interface.link
    Network File: /etc/systemd/network/50-default.network
            Type: ether
           State: routable (configured)
            Path: pci-0000:02:00.0
          Driver: igb
          Vendor: Intel Corporation
           Model: I210 Gigabit Network Connection
      HW Address: a4:bf:01:08:b5:74 (Intel Corporate)
         Address: 17.33.24.3
                  2001:41d0:8:2b88::
                  fe80::a6bf:1ff:fe08:b574
         Gateway: 17.33.24.25 (Cisco Systems, Inc)
                  20:41:8:2bff:ff:ff:ff:ff (Cisco Systems, Inc)
             DNS: 127.0.0.1
                  21.6.3.9
                  2001:41d0:3:163::1
             NTP: ntp.ovh.net
```

### redémarrer system pour la prise en compte des changements réseau

```bash
systemctl restart systemd-networkd
```

## journald

### ressources 

* https://www.linuxtricks.fr/wiki/utiliser-journalctl-les-logs-de-systemd

## afficher un surcharge dans /etc/


systemctl edit --full  sshd.service
systemctl cat --full  sshd.service

### voir les différences de conf

systemd-delta

### surcharger la conf 

systemctl edit --full varnish.service

### attendre le nfs 

systemctl enable systemd-networkd-wait-online.service && systemctl daemon-reload
