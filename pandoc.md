# Pandoc

## Migration de Mediawiki vers Github 

Nous l'installons via *cabale* 

```bash
apt-get install cabal-install
```

Nous faisons ce que le fichier INSTALL nous demande

```bash
cabal update
cabal install pandoc
 ```

Par défaut, l'executable est installé dans 

```bash
~/.cabal/bin/pandoc
```

Et enfin on transforme

```bash
pandoc -r mediawiki /tmp/in -t markdown -o /tmp/ou
````

Ou :
* -r format d'entrée
* -t format de sortie
* -o fichier de sortie

## Markdown vers texte 

```bash
pandoc -f markdown -t plain examples/work/interview1.md
```

## Mardown vers html avec toc

```bash
pandoc -f markdown -t html5 --toc -s -o ~/ipsec.html ipsec-rw.md
```
